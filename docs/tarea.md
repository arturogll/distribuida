
![unam logo](unam.svg)

# Bases de datos Distribuidas

# Alumnos

- Lopez Perez Aime Cristina
- Guerreo López Luis Arturo



<br>
<br>

# PRACTICA
Para esta practica se utilizaron tecnologias como docker,mariadb y python.

Se desplegaron tres contenedores conectados por medio de una network interna y donde python seria el encargado de controlar todo el sitema, desde el cliente de python

Laa siguiente es la estructura de las carpetas



Estructura de carpetas

- docker-compose.yml
- cliente
  - Dockerfile
  - scripts
    - conection.py
- db1
  - scripts
    - database.sql
- db2
  - scripts
    - database.sql


## docker-compose.yml

```yml
version: "3"

services:
  cliente:
    build: "./cliente"
    image: client
    networks:
      - distribuida
    volumes:
      - ./cliente/scripts:/usr/src/app
    environment:
      MYSQL1_URL: "mysql://local:123@net_db1/banco1"
      MYSQL2_URL: "mysql://local:123@net_db2/banco2"
    depends_on:
      - db1
      - db2
  db1:
    image: mariadb
    networks:
      distribuida:
        aliases:
          - net_db1
    environment:
      MYSQL_ROOT_PASSWORD: "123"
      MYSQL_DATABASE: "banco1"
      MYSQL_USER: "local"
      MYSQL_PASSWORD: "123"
      MYSQL_ALLOW_EMPTY_PASSWORD: "yes"
    volumes:
      - ./db1/scripts:/docker-entrypoint-initdb.d/
  db2:
    image: mariadb
    networks:
      distribuida:
        aliases:
          - net_db2
    environment:
      MYSQL_ROOT_PASSWORD: "123"
      MYSQL_DATABASE: "banco2"
      MYSQL_USER: "local"
      MYSQL_PASSWORD: "123"
      MYSQL_ALLOW_EMPTY_PASSWORD: "yes"
    volumes:
      - ./db2/scripts:/docker-entrypoint-initdb.d/
networks:
  distribuida:


```



## Cliente

### Dockerfile

```Dockerfile
FROM python:3

WORKDIR /usr/src/app

# COPY requirements.txt ./
RUN pip install sqlalchemy psycopg2 mysqlclient cassandra-driver
RUN apt update
RUN apt -y install vim nano
COPY ./scripts .

```
### connection.py

```python
from sqlalchemy import create_engine
import os
import argparse
parser = argparse.ArgumentParser("Modify the distributed database")
parser.add_argument("operation", help="type of operation [check|trans]")
parser.add_argument("-b",type=int ,default=0,help="select the bank [1|2]")
parser.add_argument('-s', type=int, default=0,help='user to remove money')
parser.add_argument('-e', type=int, default=0,help='user to add money')
parser.add_argument('-m', type=int, default=0,help='money to transfer')
args = parser.parse_args()

db1 =create_engine(os.environ.get('MYSQL1_URL'))
db2 =create_engine(os.environ.get('MYSQL2_URL'))
db1.connect()
db2.connect()

if args.operation == "check":
    if args.b == 1:
        rs = db1.execute('SELECT * FROM usuario')
        for row in rs:
            print(row)
    elif args.b == 2:
        rs = db2.execute('SELECT * FROM usuario')
        for row in rs:
            print(row)
    else:
        print("\n================================")
        print("DATABASE banco1 \n")
        rs = db1.execute('SELECT * FROM usuario')
        for row in rs:
            print(row)
        print("\n================================")
        print("DATABASE banco2 \n")
        rs = db2.execute('SELECT * FROM usuario')
        for row in rs:
            print(row)
        print("\n================================")
elif args.operation == "trans":
    if args.b == 1:
        rs = db1.execute('CALL trans({},-{})'.format(args.s,args.m))
        for row in rs:
            if row[0]:
                rs = db2.execute('CALL trans({},{})'.format(args.e,args.m))
                for row in rs:
                    if row[0]:
                         print("Exito al transferir")
                    else:
                        print("Error al transferir")
            else:
                print("Error al transferir")
    if args.b == 2:
        rs = db2.execute('CALL trans({},-{})'.format(args.s,args.m))
        for row in rs:
            if row[0]:
                rs = db1.execute('CALL trans({},{})'.format(args.e,args.m))
                for row in rs:
                    if row[0]:
                         print("Exito al transferir")
                    else:
                        print("Error al transferir")
            else:
                print("Error al transferir")

```
## db1
### database.sql
```sql
CREATE TABLE usuario(
idUsuario INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
nombre VARCHAR(50),
saldo INTEGER
);

INSERT INTO usuario VALUES(NULL,"ARTURO GUERRERO",1000);
INSERT INTO usuario VALUES(NULL,"AIME LOPEZ",2000);
INSERT INTO usuario VALUES(NULL,"JUAN PEREZ",3000);
INSERT INTO usuario VALUES(NULL,"FELIPE DE JESUS",4000);
INSERT INTO usuario VALUES(NULL,"IVONNE LOPEZ",5000);

# STORE PROCEDURE

DELIMITER //

CREATE PROCEDURE trans(IN id INT,IN cantidad INT)
 BEGIN
  DECLARE aux INTEGER;
  DECLARE ret BOOLEAN;
    START TRANSACTION;
    select saldo into aux from usuario where idUsuario=id;
    update usuario set saldo=aux+cantidad where idUsuario=id;


    select saldo into aux from usuario where idUsuario=id;

    IF aux>=0 THEN
      COMMIT;
      SELECT True;
    ELSE
      ROLLBACK;
      SELECT False;
    END IF;
 END;
//

DELIMITER ;

```
## db2

### database.sql

```sql
CREATE TABLE usuario(
idUsuario INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
nombre VARCHAR(50),
saldo INTEGER
);

INSERT INTO usuario VALUES(NULL,"KARLA GUERERO",1000);
INSERT INTO usuario VALUES(NULL,"PAULINA GUERRERO",2000);
INSERT INTO usuario VALUES(NULL,"ALBERTO GUERRERO",3000);
INSERT INTO usuario VALUES(NULL,"ARACELY PEREZ",4000);
INSERT INTO usuario VALUES(NULL,"GABRIEL FUENTES",5000);

# STORE PROCEDURE

DELIMITER //

CREATE PROCEDURE trans(IN id INT,IN cantidad INT)
 BEGIN
  DECLARE aux INTEGER;
  DECLARE ret BOOLEAN;
    START TRANSACTION;
    select saldo into aux from usuario where idUsuario=id;
    update usuario set saldo=aux+cantidad where idUsuario=id;


    select saldo into aux from usuario where idUsuario=id;

    IF aux>=0 THEN
      COMMIT;
      SELECT True;
    ELSE
      ROLLBACK;
      SELECT False;
    END IF;
 END;
//

DELIMITER ;


```

## Ejecucion 

Ahora para aejecutar todo el ambiente introducimos en la consola

```
docker-compose run cliente bash
```
El cual nos regresara un bash y podremos ver lo que contiene el directorio

![alt](1.png)

En este momento sera posible ejecutar los sigientes comandos

```
python conection.py check
```
El cual proyectara las dos tablas de las bases de datos

![alt](2.png)

Para hacer uan transferencia de un banco a otro introduciremos el siguente comando

```
python conection.py trans -b=1 -s=1 -e=1 -m=500
```
Donde:
- -b Es el banco de donde empezara la transacción
- -s Es el id del usuario al que se le restara el dinero
- -e Es el usuario al cual se le sumara la cantidad
- -m Es la cantidad de dinero


![alt](3.png)

Como vemos nos imprimio una bandera que nos corrobora que todo se ejecuto a la perfeccion.

Ahora verificaremos la base de datos de ambos bancos.

![alt](4.png)

Vemos que la transferencia realizo que si hizo la transferencia de el usuario 1 del banco1 al usuario 1 del banco 2 por una cantidad de $500.

¿Pero que pasa si hacemos una transferncia la cual el usuario no tiene la capacidad monetaria?

![alt](5.png)

El resultado es que no puede hacer la transferencia, ahora vamos a corroborar que la base de datos de ambos bancos sigan en el mismo estado

![alt](6.png)

Como podemos observar los usuarios siguen con las mismas cantidades ya que no se pudo lograr la transferencia