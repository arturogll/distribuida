-- MySQL dump 10.17  Distrib 10.3.13-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: africa
-- ------------------------------------------------------
-- Server version	10.3.13-MariaDB-1:10.3.13+maria~bionic

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `africa`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `africa` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `africa`;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `idUsuario` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idUsuario`)
) ENGINE=InnoDB AUTO_INCREMENT=999 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (2,'Desdemona'),(5,'Kinna'),(6,'Stevena'),(10,'Kessia'),(11,'Nert'),(17,'Merry'),(19,'Viviana'),(30,'Roth'),(34,'Orson'),(36,'Allys'),(47,'Brooke'),(52,'Esme'),(54,'Janey'),(58,'Dynah'),(71,'Fiona'),(78,'Jemmy'),(80,'Ingrim'),(92,'Hortensia'),(93,'Reuben'),(98,'Daffi'),(109,'Rivy'),(118,'Cam'),(129,'Patty'),(132,'Bert'),(142,'Kendrick'),(149,'Jaquelin'),(152,'Kirsteni'),(158,'Jarid'),(163,'Julia'),(165,'Huntington'),(170,'Baillie'),(171,'Freddy'),(172,'Ira'),(174,'Kalli'),(176,'Jens'),(178,'Tootsie'),(185,'Haven'),(192,'Jeffrey'),(208,'Thelma'),(213,'Delmer'),(218,'Wilt'),(233,'Shanda'),(235,'Melita'),(243,'Samantha'),(247,'Pace'),(260,'Drusie'),(272,'Daryl'),(287,'Arvin'),(290,'Gabriella'),(295,'Binny'),(296,'Mel'),(297,'Shem'),(298,'Korney'),(303,'Ellene'),(306,'Talbert'),(308,'Flin'),(310,'Camilla'),(312,'Gilligan'),(318,'Dode'),(321,'Finley'),(322,'Anatol'),(330,'Mellicent'),(345,'Lottie'),(352,'Barrett'),(354,'Chrissie'),(358,'Gwenore'),(363,'Armando'),(367,'Corbie'),(378,'Mellisa'),(384,'Linet'),(389,'Almeria'),(390,'Bondie'),(392,'Carissa'),(395,'Tabby'),(397,'Corrie'),(398,'Giselle'),(411,'Viki'),(413,'Happy'),(419,'Cristal'),(420,'Dulce'),(423,'Pavlov'),(425,'Chrissie'),(428,'Barnett'),(433,'Martie'),(457,'Ardra'),(459,'Marcelia'),(462,'Crystal'),(463,'Wesley'),(475,'Merridie'),(476,'Meredeth'),(493,'Sukey'),(495,'Barbe'),(497,'Laina'),(508,'Elly'),(514,'Towney'),(517,'Hamlen'),(526,'Dalila'),(527,'Georas'),(534,'Kyla'),(545,'Persis'),(546,'Malena'),(548,'Zolly'),(549,'Jasmin'),(552,'Sasha'),(559,'Adorne'),(570,'Johannes'),(580,'Panchito'),(581,'Natale'),(589,'Althea'),(591,'Hadley'),(601,'Wallache'),(602,'Odessa'),(606,'Kippar'),(613,'Bridget'),(614,'Jedediah'),(623,'Hewie'),(630,'Hermina'),(632,'Ara'),(633,'Myranda'),(636,'Loydie'),(638,'Hildagarde'),(642,'Stevie'),(645,'Shelba'),(662,'Krissie'),(671,'Dudley'),(677,'Pru'),(681,'Dolly'),(693,'Gilles'),(697,'Marian'),(703,'Mallory'),(717,'Benedikt'),(732,'Karney'),(735,'Kort'),(751,'Heath'),(759,'Brandais'),(766,'Deanna'),(795,'Luz'),(797,'Nara'),(798,'Lurette'),(799,'Rufus'),(801,'Vera'),(803,'Alidia'),(815,'Grete'),(821,'Mickie'),(826,'Dane'),(837,'Fara'),(853,'Tommi'),(854,'Doll'),(860,'Ivonne'),(864,'Sol'),(865,'Benyamin'),(866,'Mab'),(877,'Hendrik'),(883,'Wat'),(893,'Coralyn'),(903,'Babbie'),(906,'Madison'),(909,'Pincas'),(910,'Liuka'),(912,'Lewiss'),(913,'Elka'),(926,'Eveline'),(941,'Eve'),(942,'Barret'),(943,'Ingra'),(947,'Ed'),(954,'Fina'),(959,'Philbert'),(967,'Hedwig'),(970,'Genovera'),(977,'Ade'),(982,'Denny'),(983,'Puff'),(988,'Bobinette'),(992,'Madelina'),(998,'Constancia');
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarioViaje`
--

DROP TABLE IF EXISTS `usuarioViaje`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarioViaje` (
  `idUsuarioViaje` int(11) NOT NULL AUTO_INCREMENT,
  `idUsuario` int(11) DEFAULT NULL,
  `idViaje` int(11) DEFAULT NULL,
  PRIMARY KEY (`idUsuarioViaje`),
  KEY `idViaje` (`idViaje`),
  KEY `idUsuario` (`idUsuario`),
  CONSTRAINT `usuarioViaje_ibfk_1` FOREIGN KEY (`idViaje`) REFERENCES `viaje` (`idViaje`),
  CONSTRAINT `usuarioViaje_ibfk_2` FOREIGN KEY (`idUsuario`) REFERENCES `usuario` (`idUsuario`)
) ENGINE=InnoDB AUTO_INCREMENT=999 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarioViaje`
--

LOCK TABLES `usuarioViaje` WRITE;
/*!40000 ALTER TABLE `usuarioViaje` DISABLE KEYS */;
INSERT INTO `usuarioViaje` VALUES (22,19,545),(25,308,427),(29,413,74),(32,766,244),(34,213,388),(36,80,398),(42,322,324),(47,192,336),(49,662,693),(51,546,517),(52,475,454),(60,630,867),(62,295,439),(63,78,554),(67,493,517),(70,517,517),(71,367,796),(72,514,363),(78,977,357),(79,623,375),(98,132,203),(102,751,786),(103,570,808),(105,303,484),(106,392,795),(107,717,159),(110,109,101),(111,527,973),(120,93,226),(121,420,937),(126,158,286),(127,476,635),(147,260,500),(148,912,533),(151,883,332),(152,235,727),(155,580,684),(157,671,62),(160,345,101),(163,970,856),(176,178,74),(180,632,716),(183,983,351),(185,795,716),(187,149,645),(189,497,366),(204,185,398),(205,681,532),(210,693,424),(220,462,823),(223,358,726),(226,589,404),(235,171,689),(236,967,543),(244,614,272),(245,601,424),(247,52,48),(251,176,74),(252,759,123),(262,798,896),(288,853,424),(294,697,213),(301,837,786),(306,5,341),(307,243,823),(313,803,815),(314,165,123),(317,913,873),(323,732,408),(327,411,487),(329,893,174),(330,636,637),(335,384,894),(341,988,285),(342,318,726),(353,559,709),(355,298,459),(357,428,897),(364,297,381),(366,420,48),(368,642,288),(380,310,689),(385,459,999),(390,992,554),(391,638,381),(393,290,873),(397,910,964),(400,2,943),(410,633,579),(413,954,206),(423,866,378),(429,457,357),(430,92,644),(438,425,474),(452,247,745),(453,943,302),(454,864,486),(456,909,427),(457,580,18),(460,552,498),(465,581,485),(470,735,108),(482,942,532),(484,677,416),(506,36,964),(531,821,408),(537,208,766),(541,312,226),(542,398,906),(550,397,964),(554,306,801),(556,163,831),(558,703,484),(567,998,108),(577,645,705),(578,826,856),(584,233,644),(586,389,38),(602,287,848),(612,5,41),(615,982,168),(625,243,30),(627,602,557),(637,423,831),(638,272,294),(642,860,285),(645,545,890),(650,799,705),(654,6,206),(656,526,840),(667,534,543),(673,677,484),(676,34,874),(678,10,576),(689,591,286),(694,395,745),(696,30,709),(717,495,501),(727,17,738),(731,129,810),(733,54,333),(737,149,701),(741,58,501),(743,296,388),(744,47,485),(754,172,868),(755,170,487),(759,390,952),(766,118,992),(770,378,906),(779,363,916),(782,548,557),(784,613,398),(786,877,454),(787,71,962),(788,11,738),(793,174,474),(797,508,215),(832,352,493),(837,797,716),(838,142,75),(851,419,403),(885,354,725),(889,152,874),(894,549,547),(899,801,351),(905,321,130),(916,218,269),(930,129,58),(948,947,496),(960,606,897),(961,926,416),(963,433,871),(970,330,388),(971,98,215),(976,903,381),(978,815,612),(980,463,994),(986,906,215),(987,941,533),(988,854,952),(990,514,876),(991,959,745),(998,865,999);
/*!40000 ALTER TABLE `usuarioViaje` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `viaje`
--

DROP TABLE IF EXISTS `viaje`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `viaje` (
  `idViaje` int(11) NOT NULL AUTO_INCREMENT,
  `horaSalida` int(11) DEFAULT NULL,
  `horaLlegada` int(11) DEFAULT NULL,
  `origen` varchar(10) DEFAULT NULL,
  `destino` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`idViaje`)
) ENGINE=InnoDB AUTO_INCREMENT=1000 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `viaje`
--

LOCK TABLES `viaje` WRITE;
/*!40000 ALTER TABLE `viaje` DISABLE KEYS */;
INSERT INTO `viaje` VALUES (17,1050,3050,'africa','america'),(18,1044,3044,'africa','europa'),(30,1172,3172,'africa','asia'),(38,804,2804,'africa','africa'),(41,926,2926,'africa','europa'),(48,724,2724,'africa','africa'),(55,1121,3121,'africa','asia'),(58,1122,3122,'africa','america'),(62,879,2879,'africa','europa'),(65,856,2856,'africa','oceania'),(72,950,2950,'africa','oceania'),(74,901,2901,'africa','africa'),(75,1106,3106,'africa','europa'),(77,1144,3144,'africa','america'),(100,1077,3077,'africa','africa'),(101,1108,3108,'africa','africa'),(108,814,2814,'africa','oceania'),(114,1167,3167,'africa','oceania'),(118,919,2919,'africa','oceania'),(121,855,2855,'africa','america'),(123,1136,3136,'africa','africa'),(130,856,2856,'africa','asia'),(149,944,2944,'africa','oceania'),(159,1182,3182,'africa','africa'),(162,1030,3030,'africa','asia'),(168,741,2741,'africa','oceania'),(173,988,2988,'africa','asia'),(174,1155,3155,'africa','asia'),(195,1091,3091,'africa','oceania'),(203,744,2744,'africa','oceania'),(206,995,2995,'africa','europa'),(211,753,2753,'africa','africa'),(213,929,2929,'africa','asia'),(215,825,2825,'africa','oceania'),(223,1112,3112,'africa','asia'),(226,1018,3018,'africa','africa'),(229,1080,3080,'africa','europa'),(239,923,2923,'africa','europa'),(244,1056,3056,'africa','europa'),(257,1052,3052,'africa','oceania'),(262,1031,3031,'africa','europa'),(269,821,2821,'africa','africa'),(270,802,2802,'africa','america'),(272,790,2790,'africa','africa'),(276,854,2854,'africa','asia'),(277,948,2948,'africa','europa'),(285,936,2936,'africa','asia'),(286,1161,3161,'africa','america'),(288,1017,3017,'africa','europa'),(291,715,2715,'africa','america'),(294,897,2897,'africa','africa'),(302,958,2958,'africa','america'),(324,700,2700,'africa','africa'),(332,1067,3067,'africa','america'),(333,828,2828,'africa','oceania'),(336,1119,3119,'africa','africa'),(341,822,2822,'africa','america'),(342,932,2932,'africa','asia'),(348,731,2731,'africa','america'),(351,926,2926,'africa','asia'),(357,1172,3172,'africa','america'),(363,709,2709,'africa','asia'),(366,1055,3055,'africa','oceania'),(375,1195,3195,'africa','asia'),(378,1016,3016,'africa','oceania'),(381,1131,3131,'africa','africa'),(388,885,2885,'africa','asia'),(398,1022,3022,'africa','oceania'),(399,809,2809,'africa','oceania'),(403,1111,3111,'africa','asia'),(404,813,2813,'africa','oceania'),(408,997,2997,'africa','africa'),(410,1164,3164,'africa','america'),(413,885,2885,'africa','asia'),(416,748,2748,'africa','america'),(417,955,2955,'africa','africa'),(424,1004,3004,'africa','europa'),(427,766,2766,'africa','europa'),(436,854,2854,'africa','africa'),(439,891,2891,'africa','asia'),(440,1182,3182,'africa','oceania'),(442,962,2962,'africa','asia'),(450,976,2976,'africa','europa'),(454,724,2724,'africa','africa'),(457,984,2984,'africa','asia'),(459,1069,3069,'africa','asia'),(464,1080,3080,'africa','asia'),(470,815,2815,'africa','america'),(474,1103,3103,'africa','asia'),(481,1148,3148,'africa','america'),(484,735,2735,'africa','asia'),(485,856,2856,'africa','europa'),(486,970,2970,'africa','africa'),(487,897,2897,'africa','asia'),(493,836,2836,'africa','america'),(496,849,2849,'africa','africa'),(498,979,2979,'africa','asia'),(500,1136,3136,'africa','oceania'),(501,729,2729,'africa','america'),(507,822,2822,'africa','europa'),(517,1114,3114,'africa','america'),(529,703,2703,'africa','africa'),(532,712,2712,'africa','asia'),(533,1178,3178,'africa','oceania'),(538,951,2951,'africa','asia'),(539,1008,3008,'africa','europa'),(543,808,2808,'africa','america'),(545,1170,3170,'africa','asia'),(547,865,2865,'africa','africa'),(551,1027,3027,'africa','africa'),(554,1015,3015,'africa','africa'),(557,1155,3155,'africa','asia'),(576,776,2776,'africa','oceania'),(579,955,2955,'africa','africa'),(584,967,2967,'africa','oceania'),(587,906,2906,'africa','africa'),(612,753,2753,'africa','asia'),(618,1089,3089,'africa','america'),(625,901,2901,'africa','europa'),(626,726,2726,'africa','africa'),(635,1061,3061,'africa','africa'),(637,1024,3024,'africa','asia'),(638,795,2795,'africa','africa'),(644,1047,3047,'africa','europa'),(645,935,2935,'africa','europa'),(648,763,2763,'africa','oceania'),(684,1163,3163,'africa','europa'),(689,1123,3123,'africa','america'),(693,911,2911,'africa','asia'),(699,947,2947,'africa','oceania'),(701,832,2832,'africa','america'),(704,895,2895,'africa','europa'),(705,908,2908,'africa','asia'),(709,1191,3191,'africa','europa'),(716,1173,3173,'africa','asia'),(719,1062,3062,'africa','america'),(725,720,2720,'africa','america'),(726,1081,3081,'africa','america'),(727,899,2899,'africa','africa'),(738,750,2750,'africa','oceania'),(745,779,2779,'africa','america'),(750,945,2945,'africa','africa'),(763,1090,3090,'africa','europa'),(766,996,2996,'africa','asia'),(782,906,2906,'africa','america'),(786,784,2784,'africa','america'),(787,872,2872,'africa','europa'),(795,730,2730,'africa','oceania'),(796,754,2754,'africa','africa'),(797,873,2873,'africa','america'),(799,753,2753,'africa','europa'),(801,856,2856,'africa','asia'),(802,1023,3023,'africa','oceania'),(808,996,2996,'africa','europa'),(810,700,2700,'africa','oceania'),(815,710,2710,'africa','africa'),(816,1054,3054,'africa','africa'),(823,785,2785,'africa','africa'),(831,732,2732,'africa','africa'),(840,817,2817,'africa','america'),(848,824,2824,'africa','africa'),(856,871,2871,'africa','oceania'),(859,1041,3041,'africa','oceania'),(867,1165,3165,'africa','asia'),(868,1170,3170,'africa','oceania'),(870,1137,3137,'africa','america'),(871,1030,3030,'africa','africa'),(873,795,2795,'africa','europa'),(874,720,2720,'africa','europa'),(876,1016,3016,'africa','oceania'),(879,972,2972,'africa','africa'),(888,1183,3183,'africa','africa'),(890,991,2991,'africa','europa'),(894,1139,3139,'africa','america'),(895,942,2942,'africa','europa'),(896,950,2950,'africa','america'),(897,1192,3192,'africa','africa'),(906,986,2986,'africa','asia'),(916,966,2966,'africa','asia'),(937,866,2866,'africa','europa'),(943,877,2877,'africa','europa'),(949,791,2791,'africa','europa'),(952,1110,3110,'africa','africa'),(953,906,2906,'africa','oceania'),(962,1093,3093,'africa','america'),(964,1111,3111,'africa','asia'),(973,912,2912,'africa','europa'),(989,1103,3103,'africa','asia'),(990,859,2859,'africa','america'),(991,765,2765,'africa','africa'),(992,721,2721,'africa','asia'),(994,1155,3155,'africa','africa'),(999,1126,3126,'africa','europa');
/*!40000 ALTER TABLE `viaje` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-06-07  7:06:20
