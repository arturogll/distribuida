CREATE USER 'replication_america'@'%' IDENTIFIED BY '123';
GRANT REPLICATION SLAVE ON *.* TO 'replication_america'@'%';

CREATE USER 'replication_asia'@'%' IDENTIFIED BY '123';
GRANT REPLICATION SLAVE ON *.* TO 'replication_asia'@'%';

CREATE USER 'replication_europa'@'%' IDENTIFIED BY '123';
GRANT REPLICATION SLAVE ON *.* TO 'replication_europa'@'%';

CREATE USER 'replication_oceania'@'%' IDENTIFIED BY '123';
GRANT REPLICATION SLAVE ON *.* TO 'replication_oceania'@'%';

CREATE USER 'replication_africa'@'%' IDENTIFIED BY '123';
GRANT REPLICATION SLAVE ON *.* TO 'replication_africa'@'%';

FLUSH PRIVILEGES;
FLUSH TABLES WITH READ LOCK;

UNLOCK TABLES;

CREATE TABLE usuario(
idUsuario INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
nombre VARCHAR(50)
);

CREATE TABLE viaje(
idViaje INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
horaSalida INTEGER,
horaLlegada INTEGER,
origen VARCHAR(10),
destino VARCHAR(10)
);

CREATE TABLE usuarioViaje(
idUsuarioViaje INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
idUsuario INTEGER,
idViaje INTEGER,
FOREIGN KEY (idViaje) REFERENCES viaje(idViaje),
FOREIGN KEY (idUsuario) REFERENCES usuario(idUsuario)
);

LOAD DATA INFILE '/docker-entrypoint-initdb.d/usuarios.csv' INTO TABLE usuario FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n' SET idUsuario=null;
LOAD DATA INFILE '/docker-entrypoint-initdb.d/vuelos.csv' INTO TABLE viaje FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n' SET idViaje=null;
LOAD DATA INFILE '/docker-entrypoint-initdb.d/relacion.csv' INTO TABLE usuarioViaje FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n' SET idUsuarioViaje=null;

