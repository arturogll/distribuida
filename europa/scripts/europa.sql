-- MySQL dump 10.17  Distrib 10.3.13-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: europa
-- ------------------------------------------------------
-- Server version	10.3.13-MariaDB-1:10.3.13+maria~bionic

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `europa`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `europa` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `europa`;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `idUsuario` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idUsuario`)
) ENGINE=InnoDB AUTO_INCREMENT=1001 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (1,'Son'),(18,'Traci'),(19,'Viviana'),(28,'Conny'),(40,'Kaylee'),(48,'Pier'),(61,'Hyacinthia'),(66,'Rebecka'),(69,'Wilhelmina'),(78,'Jemmy'),(84,'Ivette'),(102,'Dolley'),(104,'Patricio'),(105,'Anatol'),(116,'Jose'),(123,'Bibi'),(126,'Brandea'),(132,'Bert'),(133,'Katherina'),(136,'Viva'),(138,'Cal'),(140,'Karel'),(165,'Huntington'),(166,'Rudiger'),(168,'Casey'),(169,'Marshal'),(189,'Tybalt'),(190,'Dill'),(198,'Rustin'),(204,'Brendan'),(211,'Rick'),(213,'Delmer'),(215,'Caria'),(224,'Bel'),(226,'Miles'),(233,'Shanda'),(241,'Patric'),(244,'Pacorro'),(246,'Barry'),(251,'Maxy'),(257,'Dwight'),(260,'Drusie'),(267,'Gale'),(268,'Dur'),(274,'Fonz'),(275,'Ree'),(280,'Pearla'),(284,'Evelina'),(289,'Jacinta'),(294,'Cirstoforo'),(295,'Binny'),(303,'Ellene'),(305,'Anselma'),(306,'Talbert'),(308,'Flin'),(314,'Fina'),(316,'Del'),(318,'Dode'),(322,'Anatol'),(328,'Deni'),(329,'Lian'),(332,'Goldia'),(335,'Eunice'),(338,'Harley'),(342,'Bran'),(344,'Cecile'),(346,'Judith'),(354,'Chrissie'),(358,'Gwenore'),(359,'Carmella'),(361,'Lillis'),(370,'Ody'),(374,'Anallese'),(380,'Pippy'),(385,'Hurleigh'),(393,'Florina'),(394,'Hedvige'),(397,'Corrie'),(402,'Marietta'),(406,'Tommi'),(407,'Jenilee'),(431,'Townsend'),(432,'Herschel'),(434,'Robinia'),(435,'Swen'),(442,'Trev'),(446,'Graig'),(451,'Lelah'),(463,'Wesley'),(464,'Virge'),(465,'Pren'),(466,'Sarena'),(470,'Michele'),(476,'Meredeth'),(494,'Micky'),(498,'Cory'),(501,'Blondell'),(502,'Isabelle'),(506,'Irina'),(514,'Towney'),(516,'Emmalee'),(521,'Fairfax'),(522,'Eugen'),(524,'Othelia'),(525,'Annabel'),(527,'Georas'),(534,'Kyla'),(537,'Tyson'),(542,'Corette'),(561,'Mortimer'),(568,'Elwin'),(573,'Say'),(575,'Tomasine'),(576,'Chane'),(578,'Daffy'),(586,'Birdie'),(593,'Giffer'),(602,'Odessa'),(604,'Faunie'),(605,'Sam'),(606,'Kippar'),(607,'Arabela'),(612,'Carly'),(625,'Caria'),(626,'Shaughn'),(632,'Ara'),(634,'Christoforo'),(644,'Marlo'),(645,'Shelba'),(646,'Portia'),(653,'Micky'),(659,'Guss'),(662,'Krissie'),(670,'Cammy'),(674,'Ole'),(681,'Dolly'),(685,'Waite'),(687,'Cassy'),(689,'Wesley'),(693,'Gilles'),(695,'Walt'),(698,'Jard'),(707,'Brent'),(713,'Othello'),(716,'Bobbe'),(718,'Freddie'),(722,'Mike'),(729,'Roberto'),(734,'Devland'),(735,'Kort'),(741,'Kamillah'),(742,'Anissa'),(743,'Pammy'),(751,'Heath'),(753,'Ilaire'),(765,'Missie'),(768,'Veriee'),(777,'Ezekiel'),(778,'Alexandros'),(782,'Valentine'),(791,'Jojo'),(792,'Riley'),(812,'Jo ann'),(815,'Grete'),(816,'Rustin'),(827,'Scottie'),(828,'Ricoriki'),(829,'Doretta'),(831,'Jo'),(836,'Jonis'),(837,'Fara'),(841,'Dona'),(847,'Sidnee'),(864,'Sol'),(865,'Benyamin'),(869,'Farrel'),(870,'Webster'),(880,'Con'),(881,'Corbett'),(887,'Alejandrina'),(894,'Elise'),(895,'Lynn'),(914,'Sloane'),(920,'Anne-corinne'),(924,'Brig'),(927,'Quentin'),(934,'Clemmie'),(957,'Myrvyn'),(960,'Roosevelt'),(961,'Jacquelyn'),(964,'Constantina'),(966,'Maximilien'),(967,'Hedwig'),(970,'Genovera'),(973,'Biddy'),(979,'Elsworth'),(983,'Puff'),(997,'Dulcinea'),(1000,'Sanford');
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarioViaje`
--

DROP TABLE IF EXISTS `usuarioViaje`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarioViaje` (
  `idUsuarioViaje` int(11) NOT NULL AUTO_INCREMENT,
  `idUsuario` int(11) DEFAULT NULL,
  `idViaje` int(11) DEFAULT NULL,
  PRIMARY KEY (`idUsuarioViaje`),
  KEY `idViaje` (`idViaje`),
  KEY `idUsuario` (`idUsuario`),
  CONSTRAINT `usuarioViaje_ibfk_1` FOREIGN KEY (`idViaje`) REFERENCES `viaje` (`idViaje`),
  CONSTRAINT `usuarioViaje_ibfk_2` FOREIGN KEY (`idUsuario`) REFERENCES `usuario` (`idUsuario`)
) ENGINE=InnoDB AUTO_INCREMENT=995 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarioViaje`
--

LOCK TABLES `usuarioViaje` WRITE;
/*!40000 ALTER TABLE `usuarioViaje` DISABLE KEYS */;
INSERT INTO `usuarioViaje` VALUES (6,166,938),(9,576,900),(11,215,178),(12,768,717),(14,586,278),(33,894,433),(45,920,473),(46,241,818),(50,612,924),(55,607,61),(58,521,64),(64,40,927),(73,136,927),(76,722,185),(82,40,172),(84,812,92),(86,451,278),(87,865,821),(89,967,695),(92,687,265),(95,226,431),(99,431,837),(108,322,76),(115,966,631),(116,506,521),(123,165,415),(136,847,938),(141,514,278),(142,920,310),(164,465,971),(167,69,758),(171,707,384),(173,841,231),(175,698,697),(178,289,736),(179,632,421),(186,463,770),(195,257,632),(199,645,411),(202,306,220),(207,28,927),(208,61,216),(209,573,534),(211,870,385),(212,329,695),(216,393,671),(222,634,519),(225,224,995),(228,251,821),(232,19,951),(233,84,520),(234,359,860),(238,777,629),(248,274,834),(257,829,853),(258,753,397),(259,828,757),(260,267,61),(266,66,960),(273,525,268),(282,470,900),(284,316,125),(285,432,23),(290,260,818),(292,927,330),(295,646,928),(316,464,389),(328,527,837),(333,674,650),(334,394,293),(345,869,61),(346,695,156),(348,537,757),(351,957,691),(354,741,760),(356,190,694),(361,132,42),(372,105,770),(373,791,860),(379,132,405),(382,342,961),(383,332,473),(387,734,854),(399,670,985),(407,78,152),(412,189,310),(415,198,246),(420,168,963),(428,662,945),(432,257,718),(444,964,293),(458,815,565),(464,681,147),(466,713,508),(469,498,178),(479,385,880),(483,575,589),(498,831,601),(500,407,125),(501,729,76),(502,924,428),(505,435,534),(516,322,671),(517,213,825),(520,48,469),(521,446,676),(523,735,434),(543,605,647),(544,537,564),(546,501,970),(551,370,825),(555,138,671),(564,123,880),(565,593,443),(568,792,948),(575,294,43),(580,606,647),(585,687,140),(591,133,508),(592,1000,903),(593,268,405),(596,470,430),(599,233,938),(604,751,42),(605,358,676),(606,602,502),(607,895,140),(609,685,220),(618,837,11),(619,434,389),(623,303,697),(628,914,502),(633,578,697),(634,116,862),(641,434,621),(643,604,490),(651,534,167),(653,625,22),(655,136,742),(658,1,22),(659,346,935),(661,524,411),(664,104,779),(666,318,328),(668,314,153),(671,827,397),(674,338,601),(675,476,911),(682,466,421),(684,169,996),(687,695,697),(699,689,767),(701,465,261),(704,275,694),(705,881,647),(706,778,328),(712,280,758),(714,743,178),(723,718,519),(734,970,697),(740,442,927),(745,344,22),(752,133,843),(756,831,138),(761,328,718),(764,979,928),(765,246,965),(768,561,261),(771,626,180),(790,204,866),(795,516,578),(799,78,28),(800,244,760),(802,568,948),(805,542,956),(807,880,28),(809,693,145),(814,126,881),(820,305,621),(828,887,495),(829,644,153),(833,659,944),(836,18,574),(849,765,866),(859,406,742),(864,869,3),(865,960,148),(868,397,214),(876,816,431),(883,502,433),(892,284,278),(893,335,965),(898,782,917),(901,934,152),(902,522,941),(907,402,420),(908,864,519),(912,84,741),(917,354,185),(918,102,183),(922,836,753),(935,308,742),(936,964,909),(938,295,502),(940,997,282),(949,742,881),(951,653,148),(952,374,561),(957,211,216),(962,983,411),(967,140,182),(968,361,866),(972,973,216),(973,1000,253),(981,961,574),(989,716,282),(993,380,349),(994,494,860);
/*!40000 ALTER TABLE `usuarioViaje` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `viaje`
--

DROP TABLE IF EXISTS `viaje`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `viaje` (
  `idViaje` int(11) NOT NULL AUTO_INCREMENT,
  `horaSalida` int(11) DEFAULT NULL,
  `horaLlegada` int(11) DEFAULT NULL,
  `origen` varchar(10) DEFAULT NULL,
  `destino` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`idViaje`)
) ENGINE=InnoDB AUTO_INCREMENT=998 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `viaje`
--

LOCK TABLES `viaje` WRITE;
/*!40000 ALTER TABLE `viaje` DISABLE KEYS */;
INSERT INTO `viaje` VALUES (3,1091,3091,'europa','oceania'),(5,913,2913,'europa','asia'),(11,980,2980,'europa','oceania'),(19,790,2790,'europa','asia'),(22,1154,3154,'europa','oceania'),(23,777,2777,'europa','africa'),(26,1061,3061,'europa','oceania'),(28,751,2751,'europa','europa'),(42,869,2869,'europa','oceania'),(43,883,2883,'europa','africa'),(45,1144,3144,'europa','america'),(52,1067,3067,'europa','america'),(61,1171,3171,'europa','africa'),(64,1019,3019,'europa','oceania'),(76,842,2842,'europa','america'),(89,1182,3182,'europa','america'),(92,955,2955,'europa','america'),(94,911,2911,'europa','america'),(98,800,2800,'europa','asia'),(111,849,2849,'europa','asia'),(125,1118,3118,'europa','europa'),(128,882,2882,'europa','asia'),(138,885,2885,'europa','oceania'),(140,703,2703,'europa','america'),(141,1145,3145,'europa','america'),(145,1028,3028,'europa','europa'),(146,885,2885,'europa','africa'),(147,934,2934,'europa','america'),(148,1015,3015,'europa','oceania'),(151,928,2928,'europa','africa'),(152,795,2795,'europa','asia'),(153,1093,3093,'europa','africa'),(156,1049,3049,'europa','africa'),(157,1051,3051,'europa','oceania'),(167,779,2779,'europa','america'),(172,714,2714,'europa','asia'),(177,831,2831,'europa','europa'),(178,1137,3137,'europa','asia'),(179,924,2924,'europa','europa'),(180,854,2854,'europa','asia'),(182,759,2759,'europa','asia'),(183,1086,3086,'europa','oceania'),(185,1161,3161,'europa','africa'),(197,736,2736,'europa','oceania'),(207,1111,3111,'europa','asia'),(210,1190,3190,'europa','europa'),(212,939,2939,'europa','asia'),(214,759,2759,'europa','oceania'),(216,716,2716,'europa','asia'),(218,1093,3093,'europa','oceania'),(220,1023,3023,'europa','america'),(225,728,2728,'europa','asia'),(231,949,2949,'europa','asia'),(246,819,2819,'europa','africa'),(253,847,2847,'europa','asia'),(261,710,2710,'europa','europa'),(265,870,2870,'europa','oceania'),(268,1145,3145,'europa','asia'),(271,955,2955,'europa','asia'),(278,806,2806,'europa','asia'),(282,1187,3187,'europa','asia'),(290,866,2866,'europa','europa'),(293,807,2807,'europa','asia'),(306,774,2774,'europa','europa'),(310,1136,3136,'europa','oceania'),(328,749,2749,'europa','europa'),(330,1127,3127,'europa','europa'),(334,749,2749,'europa','africa'),(349,754,2754,'europa','oceania'),(364,1080,3080,'europa','africa'),(380,960,2960,'europa','oceania'),(384,821,2821,'europa','oceania'),(385,862,2862,'europa','europa'),(386,1124,3124,'europa','oceania'),(389,1066,3066,'europa','asia'),(397,1016,3016,'europa','oceania'),(405,743,2743,'europa','america'),(409,736,2736,'europa','europa'),(411,779,2779,'europa','oceania'),(415,1148,3148,'europa','asia'),(420,1029,3029,'europa','asia'),(421,1161,3161,'europa','africa'),(428,942,2942,'europa','oceania'),(430,1186,3186,'europa','oceania'),(431,1158,3158,'europa','africa'),(433,823,2823,'europa','america'),(434,809,2809,'europa','america'),(441,908,2908,'europa','america'),(443,1194,3194,'europa','africa'),(447,929,2929,'europa','asia'),(462,994,2994,'europa','europa'),(469,1081,3081,'europa','europa'),(473,1008,3008,'europa','asia'),(490,997,2997,'europa','asia'),(491,820,2820,'europa','america'),(492,808,2808,'europa','asia'),(495,1046,3046,'europa','europa'),(502,943,2943,'europa','europa'),(508,756,2756,'europa','oceania'),(519,988,2988,'europa','europa'),(520,1078,3078,'europa','america'),(521,1028,3028,'europa','asia'),(528,1025,3025,'europa','africa'),(534,1066,3066,'europa','america'),(542,1051,3051,'europa','africa'),(561,1107,3107,'europa','oceania'),(564,995,2995,'europa','america'),(565,1069,3069,'europa','america'),(566,861,2861,'europa','europa'),(568,893,2893,'europa','america'),(574,715,2715,'europa','america'),(578,808,2808,'europa','asia'),(589,853,2853,'europa','asia'),(591,1038,3038,'europa','asia'),(601,773,2773,'europa','america'),(604,831,2831,'europa','oceania'),(609,912,2912,'europa','europa'),(621,964,2964,'europa','europa'),(629,868,2868,'europa','oceania'),(631,848,2848,'europa','asia'),(632,1047,3047,'europa','oceania'),(646,861,2861,'europa','asia'),(647,1136,3136,'europa','africa'),(650,1150,3150,'europa','oceania'),(657,1114,3114,'europa','america'),(671,1110,3110,'europa','europa'),(676,1079,3079,'europa','asia'),(691,767,2767,'europa','europa'),(694,1175,3175,'europa','europa'),(695,739,2739,'europa','oceania'),(697,1003,3003,'europa','africa'),(711,914,2914,'europa','america'),(712,1086,3086,'europa','asia'),(717,990,2990,'europa','europa'),(718,826,2826,'europa','america'),(732,995,2995,'europa','europa'),(736,728,2728,'europa','oceania'),(741,952,2952,'europa','america'),(742,910,2910,'europa','america'),(744,1070,3070,'europa','africa'),(753,858,2858,'europa','europa'),(757,729,2729,'europa','asia'),(758,1147,3147,'europa','america'),(760,841,2841,'europa','america'),(767,914,2914,'europa','asia'),(770,705,2705,'europa','europa'),(774,981,2981,'europa','america'),(777,813,2813,'europa','europa'),(779,1154,3154,'europa','oceania'),(791,1021,3021,'europa','asia'),(818,927,2927,'europa','asia'),(821,1064,3064,'europa','africa'),(825,806,2806,'europa','africa'),(826,1013,3013,'europa','africa'),(834,871,2871,'europa','europa'),(837,768,2768,'europa','oceania'),(843,1161,3161,'europa','america'),(845,873,2873,'europa','asia'),(846,866,2866,'europa','africa'),(849,927,2927,'europa','america'),(853,700,2700,'europa','europa'),(854,955,2955,'europa','oceania'),(860,732,2732,'europa','africa'),(862,1085,3085,'europa','oceania'),(864,1090,3090,'europa','europa'),(866,1113,3113,'europa','europa'),(875,786,2786,'europa','africa'),(880,1053,3053,'europa','asia'),(881,1131,3131,'europa','oceania'),(887,1066,3066,'europa','oceania'),(900,1156,3156,'europa','oceania'),(903,1137,3137,'europa','europa'),(904,1100,3100,'europa','america'),(909,904,2904,'europa','oceania'),(911,1030,3030,'europa','europa'),(917,995,2995,'europa','asia'),(919,1067,3067,'europa','asia'),(924,808,2808,'europa','europa'),(927,1041,3041,'europa','oceania'),(928,1158,3158,'europa','oceania'),(931,720,2720,'europa','europa'),(933,1081,3081,'europa','asia'),(935,1014,3014,'europa','oceania'),(938,870,2870,'europa','europa'),(939,1030,3030,'europa','oceania'),(941,766,2766,'europa','asia'),(944,1011,3011,'europa','oceania'),(945,991,2991,'europa','oceania'),(948,740,2740,'europa','europa'),(951,879,2879,'europa','africa'),(956,1134,3134,'europa','oceania'),(957,964,2964,'europa','oceania'),(960,863,2863,'europa','oceania'),(961,804,2804,'europa','america'),(963,853,2853,'europa','oceania'),(965,923,2923,'europa','america'),(970,723,2723,'europa','america'),(971,1178,3178,'europa','asia'),(975,908,2908,'europa','asia'),(985,1016,3016,'europa','africa'),(995,708,2708,'europa','africa'),(996,805,2805,'europa','asia'),(997,938,2938,'europa','asia');
/*!40000 ALTER TABLE `viaje` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-06-07  7:07:32
