SELECT * FROM viaje;

SELECT destino,count(*) FROM viaje GROUP BY destino;

SELECT u.nombre,count(*) from usuarioViaje v join usuario u on v.idUsuario = u.idUsuario group by u.nombre;

SELECT u.nombre from usuarioViaje v join usuario u on v.idUsuario = u.idUsuario where v.idViaje=8;

SELECT destino,count(*) FROM viaje GROUP BY origen;



# respaldo distribuido
# america
SELECT * FROM viaje WHERE origen='america'
INTO OUTFILE '/docker-entrypoint-initdb.d/america_viaje.csv'
FIELDS TERMINATED BY ','
LINES TERMINATED BY '\n';

SELECT u.* FROM viaje v join usuarioViaje u join usuario usr on v.idViaje=u.idViaje AND u.idUsuario=usr.idUsuario where v.origen="america" 
INTO OUTFILE '/docker-entrypoint-initdb.d/america_relacion.csv'
FIELDS TERMINATED BY ','
LINES TERMINATED BY '\n';

SELECT DISTINCT usr.* FROM viaje v join usuarioViaje u join usuario usr on v.idViaje=u.idViaje AND u.idUsuario=usr.idUsuario where v.origen="america"
INTO OUTFILE '/docker-entrypoint-initdb.d/america_usuario.csv'
FIELDS TERMINATED BY ','
LINES TERMINATED BY '\n';

# europa

SELECT * FROM viaje WHERE origen='europa'
INTO OUTFILE '/docker-entrypoint-initdb.d/europa_viaje.csv'
FIELDS TERMINATED BY ','
LINES TERMINATED BY '\n';

SELECT u.* FROM viaje v join usuarioViaje u join usuario usr on v.idViaje=u.idViaje AND u.idUsuario=usr.idUsuario where v.origen="europa"
INTO OUTFILE '/docker-entrypoint-initdb.d/europa_relacion.csv'
FIELDS TERMINATED BY ','
LINES TERMINATED BY '\n';

SELECT DISTINCT usr.* FROM viaje v join usuarioViaje u join usuario usr on v.idViaje=u.idViaje AND u.idUsuario=usr.idUsuario where v.origen="europa"
INTO OUTFILE '/docker-entrypoint-initdb.d/europa_usuario.csv'
FIELDS TERMINATED BY ','
LINES TERMINATED BY '\n';

# africa

SELECT * FROM viaje WHERE origen='africa'
INTO OUTFILE '/docker-entrypoint-initdb.d/africa_viaje.csv'
FIELDS TERMINATED BY ','
LINES TERMINATED BY '\n';

SELECT u.* FROM viaje v join usuarioViaje u join usuario usr on v.idViaje=u.idViaje AND u.idUsuario=usr.idUsuario where v.origen="africa"
INTO OUTFILE '/docker-entrypoint-initdb.d/africa_relacion.csv'
FIELDS TERMINATED BY ','
LINES TERMINATED BY '\n';

SELECT DISTINCT usr.* FROM viaje v join usuarioViaje u join usuario usr on v.idViaje=u.idViaje AND u.idUsuario=usr.idUsuario where v.origen="africa"
INTO OUTFILE '/docker-entrypoint-initdb.d/africa_usuario.csv'
FIELDS TERMINATED BY ','
LINES TERMINATED BY '\n';


#oceania

SELECT * FROM viaje WHERE origen='oceania'
INTO OUTFILE '/docker-entrypoint-initdb.d/oceania_viaje.csv'
FIELDS TERMINATED BY ','
LINES TERMINATED BY '\n';

SELECT u.* FROM viaje v join usuarioViaje u join usuario usr on v.idViaje=u.idViaje AND u.idUsuario=usr.idUsuario where v.origen="oceania"
INTO OUTFILE '/docker-entrypoint-initdb.d/oceania_relacion.csv'
FIELDS TERMINATED BY ','
LINES TERMINATED BY '\n';

SELECT DISTINCT usr.* FROM viaje v join usuarioViaje u join usuario usr on v.idViaje=u.idViaje AND u.idUsuario=usr.idUsuario where v.origen="oceania"
INTO OUTFILE '/docker-entrypoint-initdb.d/oceania_usuario.csv'
FIELDS TERMINATED BY ','
LINES TERMINATED BY '\n';

# asia

SELECT * FROM viaje WHERE origen='asia'
INTO OUTFILE '/docker-entrypoint-initdb.d/asia_viaje.csv'
FIELDS TERMINATED BY ','
LINES TERMINATED BY '\n';

SELECT u.* FROM viaje v join usuarioViaje u join usuario usr on v.idViaje=u.idViaje AND u.idUsuario=usr.idUsuario where v.origen="asia"
INTO OUTFILE '/docker-entrypoint-initdb.d/asia_relacion.csv'
FIELDS TERMINATED BY ','
LINES TERMINATED BY '\n';

SELECT DISTINCT usr.* FROM viaje v join usuarioViaje u join usuario usr on v.idViaje=u.idViaje AND u.idUsuario=usr.idUsuario where v.origen="asia"
INTO OUTFILE '/docker-entrypoint-initdb.d/asia_usuario.csv'
FIELDS TERMINATED BY ','
LINES TERMINATED BY '\n';





