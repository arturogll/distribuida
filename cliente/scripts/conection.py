from sqlalchemy import create_engine
import os
import argparse
parser = argparse.ArgumentParser("Modify the distributed database")
parser.add_argument("operation", help="type of operation [check|trans]")
parser.add_argument("-b",type=int ,default=0,help="select the bank [1|2]")
parser.add_argument('-s', type=int, default=0,help='user to remove money')
parser.add_argument('-e', type=int, default=0,help='user to add money')
parser.add_argument('-m', type=int, default=0,help='money to transfer')
args = parser.parse_args()

db1 =create_engine(os.environ.get('MYSQL1_URL'))
db2 =create_engine(os.environ.get('MYSQL2_URL'))
db1.connect()
db2.connect()

if args.operation == "check":
    if args.b == 1:
        rs = db1.execute('SELECT * FROM usuario')
        for row in rs:
            print(row)
    elif args.b == 2:
        rs = db2.execute('SELECT * FROM usuario')
        for row in rs:
            print(row)
    else:
        print("\n================================")
        print("DATABASE banco1 \n")
        rs = db1.execute('SELECT * FROM usuario')
        for row in rs:
            print(row)
        print("\n================================")
        print("DATABASE banco2 \n")
        rs = db2.execute('SELECT * FROM usuario')
        for row in rs:
            print(row)
        print("\n================================")
elif args.operation == "trans":
    if args.b == 1:
        rs = db1.execute('CALL trans({},-{})'.format(args.s,args.m))
        for row in rs:
            if row[0]:
                rs = db2.execute('CALL trans({},{})'.format(args.e,args.m))
                for row in rs:
                    if row[0]:
                         print("Exito al transferir")
                    else:
                        print("Error al transferir")
            else:
                print("Error al transferir")
    if args.b == 2:
        rs = db2.execute('CALL trans({},-{})'.format(args.s,args.m))
        for row in rs:
            if row[0]:
                rs = db1.execute('CALL trans({},{})'.format(args.e,args.m))
                for row in rs:
                    if row[0]:
                         print("Exito al transferir")
                    else:
                        print("Error al transferir")
            else:
                print("Error al transferir")
