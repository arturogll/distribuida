CHANGE MASTER TO
  MASTER_HOST='net_db1',
  MASTER_USER='replication_oceania',
  MASTER_PASSWORD='123',
  MASTER_PORT=3306,
  MASTER_LOG_FILE='master1-bin.000003',
  MASTER_LOG_POS=344,
  MASTER_CONNECT_RETRY=10;

START SLAVE;


CREATE TABLE usuario(
idUsuario INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
nombre VARCHAR(50)
);

CREATE TABLE viaje(
idViaje INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
horaSalida INTEGER,
horaLlegada INTEGER,
origen VARCHAR(10),
destino VARCHAR(10)
);

CREATE TABLE usuarioViaje(
idUsuarioViaje INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
idUsuario INTEGER,
idViaje INTEGER,
FOREIGN KEY (idViaje) REFERENCES viaje(idViaje),
FOREIGN KEY (idUsuario) REFERENCES usuario(idUsuario)
);

LOAD DATA INFILE '/docker-entrypoint-initdb.d/oceania_usuario.csv' INTO TABLE usuario FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n' ;

LOAD DATA INFILE '/docker-entrypoint-initdb.d/oceania_viaje.csv' INTO TABLE viaje FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n' ;

LOAD DATA INFILE '/docker-entrypoint-initdb.d/oceania_relacion.csv' INTO TABLE usuarioViaje FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n' ;



